//written by David De Leon
#include "OrderTicket.h"
#include <QDebug>

/*  int order_id, tableNum;
    double order_Time_In;
    //this vector translates to customer<MenuItem<quantity>>>
    std::vector< std::vector< std::vector<MenuItem> > > order;
    Employee emp;*/
/************************************************************************
 * constructors *
 ************************************************************************/
OrderTicket::OrderTicket(){
    std::vector<MenuItem> temp;
    order.push_back(temp);
}

OrderTicket::~OrderTicket(){
//        delete[] order;
}
/************************************************************************
 * mutators *
 ************************************************************************/
void OrderTicket::setOrderID(int idNum){
    order_id = idNum;
}

void OrderTicket::setEmp(Employee e){
    emp = e;
}

void OrderTicket::setTableNum(int num){
    tableNum = num;
}

//    void OrderTicket::setItemId(int); //should this be handled by the MenuItem
void OrderTicket::setQuantity(int cust, int itemID, int quant){

}

//    void OrderTicket::setItemDesc(std::string); //should this be handled by the MenuItem
void OrderTicket::setOrderTime(double time){
    order_Time_In = time;
}

/************************************************************************
 * accessors *
 ************************************************************************/
int OrderTicket::getOrderId(){
    return order_id;
}

int OrderTicket::getEmpNum(){
    return emp.getEmpNum();
}

Employee OrderTicket::getEmp(){
    return emp;
}

int OrderTicket::getTableNum(){
    return tableNum;
}

//    int OrderTicket::getItemId(int cust, int orderIndex);
//    int OrderTicket::getQuantity(int cust, int itemID);
//    std::string OrderTicket::getItemDesc(int cust, int orderIndex);
double OrderTicket::getOrderTime(){
    return order_Time_In;
}

/************************************************************************
 * other member functions *
 ************************************************************************/
void OrderTicket::addItemToOrder(int cust, MenuItem item){
    //UI starts at 1, decrement customer number after being recieved
    cust--;
    bool pushed = false;
    while(!pushed){
        //if the customer already exists in the ticket, add the item
        if(cust < order.size()){
            order[cust].push_back(item);
            pushed = true;
        //else create new customers until number of customers is reached
        }else{
            std::vector<MenuItem> temp;
            order.push_back(temp);
        }
    }
}

void OrderTicket::removeItem(int cust, int itemId){
//remove the menu item from order[cust] vector
//need to search the id to find the item to remove
    if(cust < order.size()){
        //Idea: search through the array, when found, move all subsequent items forward
        for(int i = 0; i <order.size(); i++){
//            if (order[cust][i].
        }
    }
}

double OrderTicket::calculateOrder(){
    double total = 0.0;
    int debug_count = 0;
    for(int unsigned cust = 0; cust < order.size(); cust++){
        for(int unsigned item = 0; item < order[cust].size(); item++){
            MenuItem temp = order[cust][item];
            total += temp.getItemPrice();
            debug_count++;
//            total += order[cust][item].getItemPrice();
            qDebug() << "Total price after " << debug_count << " items, $" << total;
//            qWarning() << "Total price after " << debug_count << " items, $" << total;
//            qCritical() << "Total price after " << debug_count << " items, $" << total;
        }
    }
    return total;
}

double OrderTicket::calculateOrder(int cust){
    if(cust >= order.size())
        return -1;
    double total = 0.0;
    for(int unsigned item = 0; item < order[cust].size(); item++){
        MenuItem temp = order[cust][item];
        total += temp.getItemPrice();
    }

    return total;
}

MIvect OrderTicket::debugOrderTicket(){
    return order;
}

MenuItem OrderTicket::getItemAt(int cust, int itemIndex){
    //segfault
    return order[cust][itemIndex];
}
