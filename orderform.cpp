//written by David De Leon
#include "OrderForm.h"
#include "OrderTicket.h"
#include "MenuItem.h"
#include "ui_OrderForm.h"
#include <string>
#include <QListWidget>
#include <QListWidgetItem>
//#include <QtTest/QTestEvent>
#include <QKeyEvent>


OrderForm::OrderForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OrderForm)
{
    ui->setupUi(this);
}

OrderForm::~OrderForm()
{
    delete ui;
}

/************************************************************************
 * Public slots
 ************************************************************************/

void OrderForm::setTable(int table, int emp){
    setEmpNum(emp);
    setTableNum(table);
    showOrderPage();
    //TODO need to setup OrderTicket item 'ticket'

}

/************************************************************************
 * Private slots
 ************************************************************************/

void OrderForm::on_AddItemButton_clicked()
{
    ui->listWidget;
    QString item = ui->MenuItemEdit->text();
    int idNum = item.toInt();
    if(item.isEmpty())
        return;
//TODO validate MenuItem
//workaround - the price is pulled from the name only when the name is int or double
    double price = item.toDouble();
    MenuItem menuItem;
    menuItem.setItemPrice(price);
    //QString id = ui->MenuItemEdit->text();

    //add the item to the GUI
    int quantity = ui->quantSpin->value();
    item += ", x" + QString::number(quantity);
    int customer = ui->custSpin->value();
    item += ", cust #" + QString::number(customer);
    new QListWidgetItem(item, ui->listWidget, 0);
    //add the item to the ticket object
    menuItem.setItemId(idNum);
    menuItem.setItemPrice(price);
    ticket.addItemToOrder(customer, menuItem);
    //recalculate order
//this doesnt work until MenuItem is fixed
    double tot = ticket.calculateOrder();
    //segfault
    MenuItem myItem = ticket.getItemAt(0, 0);
    QString debugtext = "price of item = " + QString::number(myItem.getItemPrice());
    ui->debuglineEdit->setText(debugtext);
    calculateTotals(tot);
//workaround - just pull the subtotal from the gui and add current item
//    QString subtotal = ui->SubtotalText->text();
//    subtotal.remove(0,1);
//    double subt = subtotal.toDouble();
//    calculateTotals(subt + price);

}

void OrderForm::on_RemoveItemButton_clicked()
{
//TODO crashes when there are no more items
    int row = ui->listWidget->currentRow();
    QListWidgetItem * rem = ui->listWidget->takeItem(row);
    //remove the item from the ticket
    QString item = rem->text();
    for(int i = 0; i < 0; i++){
//need to implement finding items in OrderTicket and deleting them
    }

    //recalculate order
//this doesnt work until MenuItem is fixed
    calculateTotals(ticket.calculateOrder());
//workaround - just pull the subtotal from the gui and subtract current item
//    double price = item.toDouble();
//    QString subtotal = ui->SubtotalText->text();
//    subtotal.remove(0,1);
//    double subt = subtotal.split(", ")[0].toDouble();
//    calculateTotals(subt - price);
//    delete rem;
}

void OrderForm::on_CashOutButton_clicked()
{
    //finalize the order and print recipts
}

void OrderForm::on_CancelOrderButton_clicked()
{
    //void out order, print a void recipt?
    //delete *ticket;
//TODO: delete file
}

void OrderForm::on_SaveOrderButton_clicked()
{
    //set order to standby
}

void OrderForm::on_custSelectSpin_valueChanged(int custNum)
{
    double total;
    //this will recalculate the order totals
    if(custNum == 0){
        //calculate total of entire bill
        total = ticket.calculateOrder();
    }
    else{
        //calculate total for selected customer - 1
        total = ticket.calculateOrder(custNum-1);
        //if customer number does not exist, stop processing and return
        if (total == -1)
            return;
    }
}

void OrderForm::on_testButton_clicked()
{

}

/************************************************************************
 * setters and getters
 ************************************************************************/

int OrderForm::getTableNum(){
    QString numStr = ui->tableNumLabel->text();
    int num = numStr.toInt();
    return num;
}

void OrderForm::setTableNum(int table){
    QString tableStr = QString::number(table);
    ui->tableNumLabel->setText(tableStr);
    ui->tableNumLabel->hide();
}

int OrderForm::getEmpNum(){
    QString numStr = ui->empNumLabel->text();
    int num = numStr.toInt();
    return num;
}

void OrderForm::setEmpNum(int emp){
    QString empStr = QString::number(emp);
    ui->empNumLabel->setText(empStr);
    ui->empNumLabel->hide();
}

/************************************************************************
 * other private methods
 ************************************************************************/

void OrderForm::showOrderPage(){
    int tableNo = getTableNum();
//    printf("the int passed to showOrderPage is %i\n", tableNo);
    QString tabletext = " Table #";
    tabletext += QString::number(tableNo);
//    printf("the new text is %s\n", tabletext.toUtf8().constData());
    ui->TableText->setText(tabletext);
    QString title = "Order for" + tabletext;
    setWindowTitle(title);
    this->show();

}

void OrderForm::calculateTotals(double subtotal){
    double taxed = subtotal * TAX;
    double totaled = subtotal + taxed;
    QString temp = "$" + QString::number(subtotal,'f',2);
    ui->SubtotalText->setText(temp);
    temp = "$" + QString::number(taxed,'f',2);
    ui->TaxText->setText(temp);
    temp = "$" + QString::number(totaled,'f',2);
    ui->GrantTotalText->setText(temp);
}

/************************************************************************
 * protected methods
 ************************************************************************/

bool OrderForm::eventFilter(QObject *obj, QEvent *event)
{
    if(event->type() == QEvent::KeyPress)
    {
        QKeyEvent *key = static_cast<QKeyEvent *>(event);

        if((key->key() == Qt::Key_Enter) || (key->key() == Qt::Key_Return))
        {
            //Enter or return was pressed
            on_AddItemButton_clicked();
        }
        else
        {
            return QObject::eventFilter(obj, event);
        }
        return true;
    }
    else
    {
        return QObject::eventFilter(obj, event);
    }

    return false;
}

//void OrderForm::keyPressEvent(QKeyEvent* pe)
//{
// if(pe->key() == Qt::Key_Return)
//     on_AddItemButton_clicked();
//  else
//     return QObject::eventFilter(obj, event);
//}
