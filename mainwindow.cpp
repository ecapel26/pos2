//not currently using this file

#include "MainWindow.h"
#include "TableWindow.h"
#include "ui_MainWindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->loginButton, SIGNAL(clicked()), this, SLOT(employeeLogin()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::employeeLogin(){
    TableWindow table;
    table.show();
}
